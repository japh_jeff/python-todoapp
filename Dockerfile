FROM python:3.9.2  

ADD . /app/

WORKDIR /app/

RUN pip install --upgrade pip && pip install -r requirements.txt

CMD ["python","app.py"]
